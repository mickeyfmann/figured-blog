let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .styles([
       'resources/assets/css/blog.css',
       'resources/assets/css/fontawesome.css',
   ], 'public/css/blog.css')
   .copy('node_modules/datatables.net/js/jquery.dataTables.js', 'public/js/jquery.dataTables.js')
    .copy('node_modules/datatables.net-dt/css/jquery.dataTables.css', 'public/css/jquery.dataTables.css')
    .copyDirectory('node_modules/datatables.net-dt/images', 'public/images/')
   .copyDirectory('node_modules/ckeditor', 'public/js/ckeditor');
