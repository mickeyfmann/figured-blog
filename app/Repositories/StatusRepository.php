<?php

namespace App\Repositories;

use App\Status;

class StatusRepository extends AbstractRepository
{
    /**
     * StatusRepository constructor.
     *
     * @param Status $model
     */
    public function __construct(Status $model)
    {
        parent::__construct($model);
    }

    /**
     * Returns status types for the specified entity code
     *
     * @param $code
     * @return mixed
     */
    public function getStatusesForEntity($code)
    {
        return $this->make()->whereHas('entities', function ($query) use ($code) {
            $query->where('code', $code);
        })->get();
    }
}