<?php
/**
 * Created by PhpStorm.
 * User: mfreeman
 * Date: 5/15/18
 * Time: 6:35 PM
 */

namespace App\Repositories;

use App\Comment;

/**
 * Class CommentRepository
 *
 * Comment object data layer interface
 *
 * @package App\Repositories
 */
class CommentRepository extends AbstractRepository
{
    public function __construct(Comment $model)
    {
        parent::__construct($model);
    }
}