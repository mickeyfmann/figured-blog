<?php
/**
 * Created by PhpStorm.
 * User: mfreeman
 * Date: 5/12/18
 * Time: 10:40 AM
 */

namespace App\Repositories;

use App\Contracts\RepositoryContract;

/**
 * Class AbstractRepository
 *
 * Abstract base repository functionality
 *
 * @package App\Repositories
 */
abstract class AbstractRepository implements RepositoryContract
{
    /**
     * @var mixed Repository Resource Model
     */
    protected $model;

    /**
     * AbstractRepository constructor.
     *
     * @param $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Returns object model
     *
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Finds a record
     *
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Get first or create new
     *
     * @param $attributes
     * @return mixed
     */
    public function firstOrCreate(array $attributes)
    {
        return $this->model->firstOrCreate($attributes);
    }

    /**
     * Find and return the first object with matching attributes or instantiate one.
     *
     * @param array $attributes
     * @return mixed
     */
    public function firstOrNew( array $attributes)
    {
        return $this->model->firstOrNew($attributes);
    }

    /**
     * Find by id with eager loading option
     *
     * @param $id
     * @param array $with
     * @return mixed
     */
    public function getById($id, array $with = [])
    {
        $query = $this->make($with);

        return $query->find($id);
    }

    /**
     * Get first by key/value with eager loading
     *
     * @param $key
     * @param $value
     * @param array $with
     * @return mixed
     */
    public function getFirstBy($key, $value, array $with = [])
    {
        return $this->make($with)->where($key, $value)->first();
    }

    /**
     * Get many by key/value with eager loading
     *
     * @param $key
     * @param $value
     * @param array $with
     * @return mixed
     */
    public function getManyBy($key, $value, array $with = [])
    {
        return $this->make($with)->where($key, $value)->get();
    }

    /**
     * Get first by key/value attribute array
     *
     * @param array $attributes
     * @param array $with
     * @return mixed
     */
    public function getFirstByAttributes(array $attributes, array $with = [])
    {
        $query = $this->make($with);

        foreach ($attributes as $key => $value) {
            if (is_array($value)) {
                $query->whereIn($key, $value);
            } else {
                $query->where($key, $value);
            }
        }

        return $query->first();
    }

    /**
     * Get many by key/value attribute array
     *
     * @param array $attributes
     * @param array $with
     * @return mixed
     */
    public function getManyByAttributes(array $attributes, array $with = [])
    {
        $query = $this->make($with);

        foreach ($attributes as $key => $value) {
            if (is_array($value)) {
                $query->whereIn($key, $value);
            } else {
                $query->where($key, $value);
            }
        }

        return $query->get();
    }

    /**
     * Randomly selects and returns an object model
     *
     * @param array $with
     * @return mixed
     */
    public function getRandom(array $with = [])
    {
        return $this->make($with)->orderByRaw('RAND()')->first();
    }

    /**
     * Randomly selects and returns an object model with the specified key and value combination
     *
     * @param $key
     * @param $value
     * @param array $with
     * @return mixed
     */
    public function getRandomBy($key, $value, array $with = [])
    {
        return $this->make($with)->where($key, $value)->orderByRaw('RAND()')->first();
    }

    /**
     * Randomly selects and returns an object model matching the provided key, value attributes
     *
     * @param array $attributes
     * @param array $with
     * @return mixed
     */
    public function getRandomByAttributes(array $attributes, array $with = [])
    {
        $query = $this->make($with)->orderByRaw('RAND()');

        foreach ($attributes as $key => $value) {
            if (is_array($value)) {
                $query->whereIn($key, $value);
            } else {
                $query->where($key, $value);
            }
        }

        return $query->first();
    }

    /**
     * Get all records
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Make new instance to query on
     *
     * @param array $with
     * @return mixed
     */
    public function make(array $with = [])
    {
        return $this->model->with($with);
    }

    /**
     * Make a new instance of the model
     *
     * @param array $attributes
     * @return mixed
     */
    public function newInstance(array $attributes = array())
    {
        return $this->model->newInstance($attributes);
    }

    /**
     * Create a new entity
     *
     * @param $fields
     * @return mixed
     */
    public function create(array $fields)
    {
        $model = $this->model->newInstance($fields);
        return $this->save($model);
    }

    /**
     * Destroy an entity/s
     *
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * Delete an existing model
     *
     * @return mixed
     */
    public function delete()
    {
        return $this->model->delete();
    }

    /**
     * Delete the model that is passed in.
     *
     * @param $model
     * @return mixed
     */
    public function deleteModel($model)
    {
        return $model->delete();
    }

    /**
     * Saves provided model
     *
     * @param $model
     * @return mixed
     */
    public function save($model)
    {
        $model->save();

        return $model;
    }

    /**
     * Copies provied model
     *
     * @param $model
     * @return mixed
     */
    public function replicate($model)
    {
        return $model->replicate();
    }

    /**
     * Attaches provided relation ids to a model via the provided relationship
     *
     * @param $model
     * @param $relationMethod
     * @param array $relatedIds
     * @return mixed
     */
    public function attach($model, $relationMethod, array $relatedIds = [])
    {
        return $model->$relationMethod()->attach($relatedIds);
    }

    /**
     * Saves provided relation models to a model via the provided relationship
     *
     * @param $model
     * @param $relationMethod
     * @param array $relatedModels
     * @return mixed
     */
    public function saveMany($model, $relationMethod, array $relatedModels)
    {
        return $model->$relationMethod()->saveMany($relatedModels);
    }

    /**
     * Removes all relations from the provided model and relationship
     *
     * @param $model
     * @param $relationMethod
     */
    public function detachAll($model, $relationMethod)
    {
        return $model->$relationMethod()->detach();
    }

    /**
     * Removes the provided relation ids from a model via the provided relationship
     *
     * @param $model
     * @param $relationMethod
     * @param null $relatedIds
     * @param bool $touch
     * @return mixed
     */
    public function detach($model, $relationMethod, $relatedIds = null, $touch = true)
    {
        return $model->$relationMethod()->detach($relatedIds, $touch);
    }

    /**
     * Saves a single related model to the provided model via the provided relationship
     *
     * @param $model
     * @param $relationMethod
     * @param $relatedModel
     * @return mixed
     */
    public function saveRelated($model, $relationMethod, $relatedModel)
    {
        return $model->$relationMethod()->save($relatedModel);
    }

    /**
     * Creates a related model with the provided attributes for the provided model and relationship
     *
     * @param $model
     * @param $relationMethod
     * @param array $relatedAttributes
     * @return mixed
     */
    public function createRelated($model, $relationMethod, array $relatedAttributes)
    {
        return $model->$relationMethod()->create($relatedAttributes);
    }

    /**
     * Updates provided model timestamps
     *
     * @param $model
     */
    public function touch($model)
    {
        $model->touch();
    }

    /**
     * Sync the intermediate tables with a list of IDs or collection of models.
     *
     * @param $model
     * @param $relationMethod
     * @param  \Illuminate\Database\Eloquent\Collection|array $ids
     * @param  bool $detaching
     * @return array
     */
    public function sync($model, $relationMethod, $ids, $detaching = true)
    {
        $model->$relationMethod()->sync($ids, $detaching);
    }

    /**
     * Inserts provided data via the loaded model
     *
     * @param array $data
     */
    public function insert(array $data)
    {
        $this->model->insert($data);
    }

    /**
     * Loads model with relationships
     *
     * @param $model
     * @param array $with
     * @return mixed
     */
    public function load(&$model, array $with)
    {
        return $model->load($with);
    }
}