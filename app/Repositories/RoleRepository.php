<?php

namespace App\Repositories;

use App\Role;

/**
 * Class RoleRepository
 *
 * Role object data layer
 *
 * @package App\Repositories
 */
class RoleRepository extends AbstractRepository
{
    /**
     * RoleRepository constructor.
     * @param Role $model
     */
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }
}