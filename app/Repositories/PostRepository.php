<?php

namespace App\Repositories;

use App\Category;
use App\Post;
use App\Tag;
use Carbon\Carbon;

/**
 * Class PostRepository
 *
 * Data access layer for posts
 *
 * @package App\Repositories
 */
class PostRepository extends AbstractRepository
{
    /**
     * PostRepository constructor.
     *
     * @param Post $model
     */
    public function __construct(Post $model)
    {
        parent::__construct($model);
    }

    /**
     * Returns posts that have publishing scheduled for now
     *
     * @return mxed
     */
    public function getPostsToPublish()
    {
        return $this->make()->whereNotNull('publish_at')->where('publish_at', '<=', Carbon::now());
    }

    /**
     * Returns the desired number of most recent posts
     *
     * @return mixed
     */
    public function getRecentPosts($count = 5)
    {
        return $this->make()
            ->whereHas('status', function($query) {
                $query->where('code', 'published');
            })->orderBy('created_at', 'desc')->take($count)->get();
    }

    /**
     * Returns the desired number of recent posts from the specified category
     *
     * @param Category $category
     * @param int $count
     * @return mixed
     */
    public function getPostsByCategory(Category $category, $count = 5)
    {
        return $this->make()
            ->whereHas('status', function($query) {
                $query->where('code', 'published');
            })
            ->whereHas('categories', function ($query) use ($category) {
                $query->where('id', $category->id);
            })->orderBy('created_at', 'desc')
            ->take($count)->get();
    }

    /**
     * Returns the desired number of recent posts associated with the specified tag
     *
     * @param Tag $tag
     * @param int $count
     * @return mixed
     */
    public function getPostsByTag(Tag $tag, $count = 5)
    {
        return $this->make()
            ->whereHas('status', function($query) {
                $query->where('code', 'published');
            })
            ->whereHas('tags', function ($query) use ($tag) {
                $query->where('id', $tag->id);
            })->orderBy('created_at', 'desc')
            ->take($count)->get();
    }


}