<?php

namespace App\Repositories;

use App\Category;

/**
 * Class CategoryRepository
 *
 * Data access layer for categories
 *
 * @package App\Repositories
 */
class CategoryRepository extends AbstractRepository
{
    /**
     * CategoryRepository constructor.
     *
     * @param Category $model
     */
    public function __construct(Category $model)
    {
        parent::__construct($model);
    }

    /**
     * Returns categories that have been assigned to posts
     *
     * @return mixed
     */
    public function getUsed()
    {
        return $this->make()->has('posts')->get();
    }
}