<?php

namespace App\Repositories;

use App\Tag;

/**
 * Class TagRepository
 *
 * Data access interface for tags
 *
 * @package App\Repositories
 */
class TagRepository extends AbstractRepository
{
    /**
     * TagRepository constructor.
     *
     * @param Tag $model
     */
    public function __construct(Tag $model)
    {
        parent::__construct($model);
    }

    /**
     * Returns a list of tags that have been associated with posts
     *
     * @return mixed
     */
    public function getUsed()
    {
        return $this->make()->has('posts')->get();
    }
}