<?php

namespace App\Jobs;

use App\Services\PostManagerService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

/**
 * Class PublishPosts
 *
 * Looks for posts that have scheduled publishing
 *
 * @package App\Jobs
 */
class PublishPosts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PostManagerService $postManager)
    {
        $posts = $postManager->getRepository()->getPostsToPublish();

        $posts->each(function($post) use ($postManager) {
            $postManager->updatePostStatus($post, 'published');
        });
    }
}
