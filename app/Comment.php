<?php

namespace App;

use App\Traits\HasStatus;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Comment
 *
 * Post feedback or response model
 *
 * @package App
 */
class Comment extends Model
{
    use HasStatus;

    protected $fillable = ['post_id', 'status_id', 'user_id', 'email', 'name', 'content'];

    /**
     * Comments must belong to a post
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    /**
     * Comments may belong to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
