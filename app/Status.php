<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Status
 *
 * Status objects used to indicate state for various system objects
 *
 * @package App
 */
class Status extends Model
{
    protected $fillable = ['code', 'name', 'description'];

    /**
     * A status can be assigned to many entities
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function entities()
    {
        return $this->belongsToMany('App\Entity');
    }

}
