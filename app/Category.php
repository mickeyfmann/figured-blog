<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 *
 * Grouping/sorting model for blog posts
 *
 * @package App
 */
class Category extends Model
{
    protected $fillable = ['code', 'name', 'description'];

    /**
     * A category can be assigned to many posts
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany('App\Post', 'post_categories');
    }
}
