<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 *
 * Image model for post images
 *
 * @package App
 */
class Image extends Model
{
    protected $fillable = ['filename', 'size', 'mime_type', 'extension'];

    /**
     * An image can be used in many posts
     *
     * @return $this
     */
    public function posts()
    {
        return $this->belongsToMany('App\Post', 'post_images')->withPivot('type', 'caption', 'ordinal');
    }
}
