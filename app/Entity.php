<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Entity
 *
 * System object used for type key look ups
 *
 * @package App
 */
class Entity extends Model
{
    protected $fillable = ['code', 'name', 'description'];

    /**
     * @return BelongsToMany
     */
    public function statuses()
    {
        return $this->belongsToMany('App\Status')->withPivot('ordinal');
    }
}
