<?php

/**
 * Generic Helper Methods
 */
if( ! function_exists('fieldWrapper'))
{
    function fieldWrapper($name, $label, $element, $helpText = null)
    {
        $out = '<div class="form-group';
        $out .= fieldError($name) . '">';
        $out .= fieldLabel($name, $label);
        $out .= $element;

        if (!is_null($helpText)) {
            $out .= '<p class="help-block">' . $helpText . '</p>';
        }

        $out .= '</div>';

        return $out;
    }
}

if( ! function_exists('addonWrapper'))
{
    function addonWrapper($name, $label, $element)
    {
        $out = '<div class="form-group';
        $out .= fieldError($name) . '">';
        $out .= fieldLabel($name, $label);
        $out .= '<div class="input-group">';
        $out .= $element;
        $out .= '<span class="input-group-addon"><i class="fa fa-calendar"></i></span>';
        $out .= '</div>';
        $out .= '</div>';

        return $out;
    }
}

if( ! function_exists('fieldError'))
{
    function fieldError($field)
    {
        $error = '';

        if ($errors = Session::get('errors')) {
            $formattedField = str_replace('[', '.', $field);
            $formattedField = str_replace(']', '', $formattedField);

            $error = $errors->first($formattedField) ? ' has-error' : '';
        }

        return $error;
    }
}

if( ! function_exists('fieldLabel'))
{
    function fieldLabel($name, $label)
    {
        if (is_null($label)) return '';

        $name = str_replace('[]', '', $name);

        $out = '<label for="id-field-' . $name . '" class="control-label">';
        $out .= $label . '</label>';

        return $out;
    }
}

if( ! function_exists('fieldAttributes'))
{
    function fieldAttributes($name, $attributes = array())
    {
        $name = str_replace('[]', '', $name);

        return array_merge(['id' => 'id-field-' . $name], $attributes);
    }
}
