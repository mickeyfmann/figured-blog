<?php

namespace App;

use Config;
use Illuminate\Database\Eloquent\Model;
use Merujan99\LaravelVideoEmbed\Facades\LaravelVideoEmbed;

/**
 * Class GalleryPost
 *
 * Gallery Post model
 *
 * @package App
 */
class GalleryPost extends Post
{
    /**
     * Returns display value for post type
     *
     * @return string
     */
    public function getTypeNameAttribute()
    {
        return 'Gallery';
    }

    /**
     * Returns additional validation rules for post type
     *
     * @return array
     */
    public function getValidationRules()
    {
        return ['gallery_images' => 'required'];
    }

    /**
     * Returns post type feature view for output
     *
     * @return string
     */
    public function getFeatureViewAttribute()
    {
        return 'includes.gallery-post';
    }

    /**
     * Ensures images are json encoded array
     *
     * @param $value
     */
    public function setGalleryImagesAttribute($value)
    {
        $this->attributes['gallery_images'] = json_encode($value);
    }

    /**
     * Ensures that an array is returned on the images
     *
     * @return mixed
     */
    public function getGalleryImagesAttribute()
    {
        return json_decode($this->attributes['gallery_images']);
    }


}
