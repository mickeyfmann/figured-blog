<?php

namespace App\Traits;

/**
 * Trait HasStatus
 *
 * Adds collective status methods to model objects
 *
 * @package App\Traits
 */
trait HasStatus
{
    /**
     * Objects assigned status
     *
     * @return mixed
     */
    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }
}