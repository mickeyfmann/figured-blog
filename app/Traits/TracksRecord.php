<?php

namespace App\Traits;

use App;

/**
 * Trait TracksRecord
 *
 * Trait for tracking created by and updated by attributes on affiliated models
 *
 * @package App\Traits
 */
trait TracksRecord
{
    /**
     * Track who created or updated a record
     */
    public static function bootTracksRecord()
    {
        $events = [
            'creating' => 'created_by',
            'updating' => 'updated_by'
        ];

        foreach($events as $event => $field) {
            static::$event(function($model) use ($field) {
                $model->setAttribute($field, $model->getTrackedUser());
            });
        }
    }

    /**
     * Realtionship for object creator
     *
     * @return mixed
     */
    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * Relationship for object modifier
     *
     * @return mixed
     */
    public function updatedBy()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

    /**
     * User to log on model
     *
     * @return mixed
     */
    protected function getTrackedUser()
    {
        $auth = App::make('auth');
        $db = App::make('db');

        return $auth->check() ? $auth->id() : $db->table('users')->where('name', 'Admin')->value('id');
    }
}