<?php

namespace App;

use App;
use App\Contracts\PostContract;
use App\Traits\HasStatus;
use App\Traits\TracksRecord;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Post
 *
 * Core blog post model
 *
 * @package App
 */
class Post extends Model implements PostContract
{
    use HasStatus, TracksRecord;

    protected $fillable = ['status_id', 'title', 'slug', 'content', 'feature_label', 'video_link', 'publish_at', 'featured_image', 'gallery_images'];

    /**
     * Sets tablename for all child models
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * Defines fields expecting date objects
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'publish_at'];

    /**
     * Model events for setting type
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function (Post $post) {
            $post->type = get_class($post);
        });

        static::updating(function (Post $post) {
            $post->type = get_class($post);
        });
    }

    /**
     * Static function for returning a display type
     *
     * @return string
     */
    public function getTypeNameAttribute()
    {
        return 'Standard';
    }

    /**
     * Custom model override to support returning of different model types
     *
     * @param array $attributes
     * @param null $connection
     * @return static
     */
    public function newFromBuilder($attributes = [], $connection = null)
    {
        if (!isset($attributes->type)) {
            return parent::newFromBuilder($attributes, $connection);
        }

        $model = App::make($attributes->type)->newInstance([], true);
        $model->setRawAttributes((array) $attributes, true);
        $model->setConnection($connection ?: $this->connection);

        return $model;
    }

    /**
     * Override to set slug as route key for posts
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * A blog can have many categtories
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category', 'post_categories', 'post_id', 'category_id');
    }

    /**
     * A blog can have many images
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function images()
    {
        return $this->belongsToMany('App\Image', 'post_images')->withPivot('type', 'caption', 'ordinal');
    }

    /**
     * Posts may have many comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('App\Comment', 'post_id');
    }

    /**
     * A post can be associated with many tags
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'post_tags', 'post_id', 'tag_id');
    }

    /**
     * Returns a shortened content string
     *
     * @return string
     */
    public function getShortAttribute()
    {
        return str_limit(strip_tags($this->attributes['content']), 300);
    }

    /**
     * Returns a formatted display of the created date
     *
     * @return mixed
     */
    public function getDisplayDateAttribute()
    {
        return $this->created_at->format('M jS Y');
    }

    /**
     * Returns
     * @return string
     */
    public function getAuthorAttribute()
    {
        if (empty($this->createdBy)) {
            return 'Admin';
        }

        return $this->createdBy->name;
    }

    /**
     * Returns a count of post comments
     *
     * @return int
     */
    public function getCommentCountAttribute()
    {
        return $this->comments()->count();
    }

    /**
     * Returns additional validation rules for post type
     *
     * @return array
     */
    public function getValidationRules()
    {
        return [
            'featured_image' => 'required',
        ];
    }

    /**
     * Returns view for outputting feature
     *
     * @return string
     */
    public function getFeatureViewAttribute()
    {
        return 'includes.standard-post';
    }

    /**
     * Returns view for outputting feature summary
     * @return string
     */
    public function getFeatureShortViewAttribute()
    {
        return 'includes.standard-post-short';
    }
}
