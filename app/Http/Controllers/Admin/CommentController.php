<?php
/**
 * Created by PhpStorm.
 * User: mfreeman
 * Date: 5/15/18
 * Time: 7:33 PM
 */

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Services\CommentManagerService;
use Session;

/**
 * Class CommentController
 *
 * Comment Admin
 *
 * @package App\Http\Controllers\Admin
 */
class CommentController extends Controller
{
    /**
     * @var CommentManagerService
     */
    protected $commentManager;

    /**
     * CommentController constructor.
     *
     * @param CommentManagerService $commentManager
     */
    public function __construct(CommentManagerService $commentManager)
    {
        $this->commentManager = $commentManager;
    }

    /**
     * Displays comment list
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.comments.list');
    }

    /**
     * Publish a comment
     *
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse
     */
    public function publish(Comment $comment)
    {
        $this->commentManager->updateCommentStatus($comment, 'published');
        Session::flash('message', 'Comment Published');
        return redirect()->route('admin.comments.list');
    }

    /**
     * SPAM a comment
     *
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse
     */
    public function spam(Comment $comment)
    {
        $this->commentManager->updateCommentStatus($comment, 'spam');
        Session::flash('message', 'Comment Marked as SPAM');
        return redirect()->route('admin.comments.list');
    }

    /**
     * Trash a comment
     *
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse
     */
    public function trash(Comment $comment)
    {
        $this->commentManager->updateCommentStatus($comment, 'trash');
        Session::flash('message', 'Comment Marked as Trash');
        return redirect()->route('admin.comments.list');
    }

    /**
     * Delete a comment
     *
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Comment $comment)
    {
        $this->commentManager->getRepository()->deleteModel($comment);
        Session::flash('message', 'Comment removed');
        return redirect()->route('admin.comments.list');
    }
}