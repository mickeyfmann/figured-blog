<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Post;
use App\Services\PostManagerService;
use Session;

/**
 * Class PostController
 *
 * Post management interface
 *
 * @package App\Http\Controllers\Admin
 */
class PostController extends Controller
{

    /**
     * @var PostManagerService
     */
    protected $postManager;

    /**
     * PostController constructor.
     *
     * @param PostManagerService $postManager
     */
    public function __construct(PostManagerService $postManager)
    {
        $this->postManager = $postManager;
    }

    /**
     * Loads and displays post list
     */
    public function index()
    {
        return view('admin.posts.list');
    }

    /**
     * Displays post creation form
     */
    public function create()
    {
        return view('admin.posts.create')
            ->with('route', route('admin.posts.store'))
            ->with('method', 'post');
    }

    /**
     * Stores new posts
     */
    public function store(PostRequest $request)
    {
        $this->postManager->createPost($request->all());
        Session::flash('message', 'Post Created');
        return redirect()->route('admin.posts.list');
    }

    /**
     * Displays post edit form
     */
    public function edit(Post $post)
    {
        return view('admin.posts.edit')
            ->with('route', route('admin.posts.update', $post))
            ->with('method', 'patch')
            ->with('post', $post);
    }

    /**
     * Stores post updates
     */
    public function update(PostRequest $request, Post $post)
    {
        $this->postManager->updatePost($post, $request->all());
        Session::flash('message', 'Post Updated');
        return redirect()->route('admin.posts.list');
    }

    /**
     * Removes post from database
     */
    public function destroy(Post $post)
    {
        $this->postManager->getRepository()->deleteModel($post);
        Session::flash('message', 'Post Removed');
        return redirect()->route('admin.posts.list');
    }

    /**
     * Publishes provided post
     *
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function publish(Post $post)
    {
        $this->postManager->updatePostStatus($post, 'published');
        Session::flash('message', 'Post Published');
        return redirect()->route('admin.posts.list');
    }

    /**
     * Trashes provided post
     *
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function trash(Post $post)
    {
        $this->postManager->updatePostStatus($post, 'trash');
        Session::flash('message', 'Post Trashed');
        return redirect()->route('admin.posts.list');
    }
}
