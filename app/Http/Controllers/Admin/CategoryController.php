<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Repositories\CategoryRepository;
use Session;

/**
 * Class CategoryController
 *
 * Category management interface
 *
 * @package App\Http\Controllers\Admin
 */
class CategoryController extends Controller
{
    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * CategoryController constructor.
     *
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Loads and displays category list
     */
    public function index()
    {
        return view('admin.categories.list');
    }

    /**
     * Displays category creation form
     */
    public function create()
    {
        return view('admin.categories.create')
            ->with('route', route('admin.categories.store'))
            ->with('method', 'post');
    }

    /**
     * Stores new categories
     */
    public function store(CategoryRequest $request)
    {
        $this->categoryRepository->create($request->only(['code', 'name', 'description']));
        Session::flash('message', 'Category Created');
        return redirect()->route('admin.categories.list');
    }

    /**
     * Displays category edit form
     */
    public function edit(Category $category)
    {
        return view('admin.categories.edit')
            ->with('route', route('admin.categories.update', $category))
            ->with('method', 'patch')
            ->with('category', $category);
    }

    /**
     * Stores category updates
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $category->fill($request->only(['code', 'name', 'description']));
        $this->categoryRepository->save($category);
        Session::flash('message', 'Category Updated');
        return redirect()->route('admin.categories.list');
    }

    /**
     * Removes category from database
     */
    public function destroy(Category $category)
    {
        $this->categoryRepository->deleteModel($category);
        Session::flash('message', 'Category Removed');
        return redirect()->route('admin.categories.list');
    }
}
