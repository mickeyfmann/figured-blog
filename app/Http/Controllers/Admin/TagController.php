<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\TagRequest;
use App\Repositories\TagRepository;
use App\Tag;
use Session;

/**
 * Class TagController
 *
 * Tag Admin interface
 *
 * @package App\Http\Controllers\Admin
 */
class TagController extends Controller
{
    /**
     * @var TagRepository
     */
    protected $tagRepository;

    /**
     * TagController constructor.
     *
     * @param TagRepository $tagRepository
     */
    public function __construct(TagRepository $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    /**
     * Loads and displays tag list
     */
    public function index()
    {
        return view('admin.tags.list');
    }

    /**
     * Displays tag creation form
     */
    public function create()
    {
        return view('admin.tags.create')
            ->with('route', route('admin.tags.store'))
            ->with('method', 'post');
    }

    /**
     * Stores new tag
     */
    public function store(TagRequest $request)
    {
        $this->tagRepository->create($request->only(['name']));
        Session::flash('message', 'Tag Created');
        return redirect()->route('admin.tags.list');
    }

    /**
     * Displays tag edit form
     */
    public function edit(Tag $tag)
    {
        return view('admin.tags.edit')
            ->with('route', route('admin.tags.update', $tag))
            ->with('method', 'patch')
            ->with('tag', $tag);
    }

    /**
     * Stores tag updates
     */
    public function update(TagRequest $request, Tag $tag)
    {
        $tag->name = $request->input('name');
        $this->tagRepository->save($tag);
        Session::flash('message', 'Tag Updated');
        return redirect()->route('admin.tags.list');
    }

    /**
     * Removes tag from database
     */
    public function destroy(Tag $tag)
    {
        $this->tagRepository->deleteModel($tag);
        Session::flash('message', 'Tag Removed');
        return redirect()->route('admin.tags.list');
    }
}
