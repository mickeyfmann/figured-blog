<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Category;
use DataTables;
use Illuminate\Http\Request;

/**
 * Class CategoryController
 *
 * Controller list api
 *
 * @package App\Http\Controllers\Api
 */
class CategoryController extends Controller
{
    public function index()
    {
        $query = Category::query();

        return DataTables::eloquent($query)
            ->removeColumn('description')
            ->addColumn('action', 'tables.category-actions')
            ->toJson();
    }
}
