<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Comment;
use DataTables;
use Illuminate\Http\Request;

/**
 * Class CommentController
 *
 * Comment list api
 *
 * @package App\Http\Controllers\Api
 */
class CommentController extends Controller
{
    public function index()
    {
        $query = Comment::query();

        return DataTables::eloquent($query)
            ->editColumn('post_id', function (Comment $comment) {
                return $comment->post->title;
            })
            ->editColumn('status_id', function (Comment $comment) {
                return $comment->status->name;
            })
            ->editColumn('user_id', function(Comment $comment) {
                return (!empty($comment->user)) ? $comment->user->name : null;
            })
            ->addColumn('action', 'tables.comment-actions')
            ->toJson();
    }
}
