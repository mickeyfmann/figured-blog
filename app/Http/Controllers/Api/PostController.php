<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Post;
use DataTables;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $query = Post::query();

        return DataTables::eloquent($query)
            ->removeColumn('slug')
            ->removeColumn('content')
            ->removeColumn('feature_label')
            ->removeColumn('video_link')
            ->removeColumn('created_by')
            ->removeColumn('updated_by')
            ->editColumn('type', function (Post $post) {
                return $post->typeName;
            })
            ->editColumn('status_id', function (Post $post) {
                return $post->status->name;
            })
            ->addColumn('action', 'tables.post-actions')
            ->toJson();
    }
}
