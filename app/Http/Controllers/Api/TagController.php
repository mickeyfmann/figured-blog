<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Tag;
use DataTables;
use Illuminate\Http\Request;

/**
 * Class TagController
 *
 * Tag list api
 *
 * @package App\Http\Controllers\Api
 */
class TagController extends Controller
{
    public function index()
    {
        $query = Tag::query();

        return DataTables::eloquent($query)
            ->addColumn('action', 'tables.tag-actions')
            ->toJson();
    }
}
