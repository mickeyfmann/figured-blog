<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CommentRequest;
use App\Post;
use App\Repositories\PostRepository;
use App\Services\CommentManagerService;
use App\Tag;
use Auth;
use Session;

class BlogController extends Controller
{
    /**
     * @var CommentManagerService
     */
    protected $commentManager;

    /**
     * @var PostRepository
     */
    protected $postRepository;


    /**
     * BlogController constructor.
     *
     * @param CommentManagerService $commentManager
     * @param PostRepository $postRepository
     */
    public function __construct(CommentManagerService $commentManager, PostRepository $postRepository)
    {
        $this->commentManager = $commentManager;
        $this->postRepository = $postRepository;
    }

    /**
     * Displays a list of the most recent blog posts
     *
     * @return $this
     */
    public function index()
    {
        $posts = $this->postRepository->getRecentPosts();
        return view('blog.index')
            ->with('posts', $posts);
    }

    /**
     * Displays a detailed post
     *
     * @param Post $post
     * @return $this
     */
    public function post(Post $post)
    {
        $post->load(['comments.user', 'comments.status']);
        return view('blog.detail')
            ->with('post', $post);
    }

    /**
     * Displays a list of posts for the provided category
     *
     * @param Category $category
     * @return $this
     */
    public function category(Category $category)
    {
        $posts = $this->postRepository->getPostsByCategory($category);

        return view('blog.category')
            ->with('category', $category)
            ->with('posts', $posts);
    }

    /**
     * Displays a list of posts for the provided tag
     *
     * @param Tag $tag
     * @return $this
     */
    public function tag(Tag $tag)
    {
        $posts = $this->postRepository->getPostsByTag($tag);

        return view('blog.tag')
            ->with('tag', $tag)
            ->with('posts', $posts);
    }

    public function comment(CommentRequest $request, Post $post)
    {
        $userId = Auth::id();

        $this->commentManager->createComment($post, $request->all(), $userId);
        Session::flash('message', 'Comment Posted');
        return redirect()->route('blog.post', $post);
    }
}
