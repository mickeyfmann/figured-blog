<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CategoryRequest
 *
 * Request validation for creating/updating categories
 *
 * @package App\Http\Requests
 */
class CategoryRequest extends FormRequest
{
    /**
     * @var array Core rules
     */
    private $rules = [
        'name'        => 'required',
        'description' => 'required',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $category = $this->route()->parameter('category');
        $code = $this->input('code');

        if ($category == null || $category->code != $code) {
            $this->rules['code'] = 'required|unique:categories';
        } else {
            $this->rules['code'] = 'required';
        }

        return $this->rules;
    }
}
