<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CommentRequest
 *
 * Comment request validation
 *
 * @package App\Http\Requests
 */
class CommentRequest extends FormRequest
{
    protected $rules = [
        'content' => 'required'
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (auth()->check()) {
            return $this->rules;
        }

        $this->rules['email'] = 'required|email';
        $this->rules['name'] = 'required';

        return $this->rules;
    }
}
