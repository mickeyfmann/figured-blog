<?php

namespace App\Http\Requests;

use App;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PostRequest
 *
 * Request validation for creating/updating posts
 *
 * @package App\Http\Requests
 */
class PostRequest extends FormRequest
{
    /**
     * @var array Core rules
     */
    private $rules = [
        'type'    => 'required',
        'title'   => 'required',
        'content' => 'required',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $post = $this->route()->parameter('post');
        $slug = $this->input('slug');
        $type = $this->input('type');

        $model = App::make($type);

        if ($post == null || $post->slug != $slug) {
            $this->rules['slug'] = 'required|unique:posts';
        } else {
            $this->rules['slug'] = 'required';
        }

        return array_merge($this->rules, $model->getValidationRules());
    }
}
