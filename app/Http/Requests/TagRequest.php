<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class TagRequest
 *
 * Request validation for creating/updating tags
 *
 * @package App\Http\Requests
 */
class TagRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $tag = $this->route()->parameter('tag');
        $name = $this->input('name');

        if ($tag == null || $tag->name != $name) {
            return [
                'name' => 'required|unique:tags'
            ];
        } else {
            return [
                'name' => 'required|unique:tags'
            ];
        }
    }
}
