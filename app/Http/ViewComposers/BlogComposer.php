<?php
namespace App\Http\ViewComposers;

use App\Repositories\CategoryRepository;
use App\Repositories\PostRepository;
use App\Repositories\TagRepository;
use Illuminate\View\View;

/**
 * Class BlogComposer
 *
 * Composer for blog navigation
 *
 * @package App\Http\ViewComposers
 */
class BlogComposer
{
    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var PostRepository
     */
    protected $postRepository;

    /**
     * @var TagRepository
     */
    protected $tagRepository;

    /**
     * BlogComposer constructor.
     *
     * @param CategoryRepository $categoryRepository
     * @param PostRepository $postRepository
     * @param TagRepository $tagRepository
     */
    public function __construct(CategoryRepository $categoryRepository, PostRepository $postRepository, TagRepository $tagRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
        $this->tagRepository = $tagRepository;
    }

    public function compose(View $view)
    {
        $categories = $this->categoryRepository->getUsed();
        $tags = $this->tagRepository->getUsed();
        $recentPosts = $this->postRepository->getRecentPosts(3);

        $view->with('categories', $categories)
            ->with('tags', $tags)
            ->with('recentPosts', $recentPosts);
    }


}