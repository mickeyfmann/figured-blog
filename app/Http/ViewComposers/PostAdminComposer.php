<?php
namespace App\Http\ViewComposers;

use App\GalleryPost;
use App\Post;
use App\Repositories\CategoryRepository;
use App\Repositories\StatusRepository;
use App\Repositories\TagRepository;
use App\VideoPost;
use Illuminate\View\View;

/**
 * Class PostAdminComposer
 *
 * Administrative composer for post management form data loading
 *
 * @package App\Http\ViewComposers
 */
class PostAdminComposer
{
    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var StatusRepository
     */
    protected $statusRepository;

    /**
     * @var TagRepository
     */
    protected $tagRepository;

    /**
     * Post Types
     *
     * @var array
     */
    protected $types = [
        Post::class        => 'Standard',
        GalleryPost::class => 'Gallery',
        VideoPost::class   => 'Video'
    ];

    /**
     * PostAdminComposer constructor.
     *
     * @param CategoryRepository $categoryRepository
     * @param StatusRepository $statusRepository
     * @param TagRepository $tagRepository
     */
    public function __construct(CategoryRepository $categoryRepository, StatusRepository $statusRepository, TagRepository $tagRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->statusRepository = $statusRepository;
        $this->tagRepository = $tagRepository;
    }

    public function compose(View $view)
    {
        $categories = $this->categoryRepository->getAll();
        $tags = $this->tagRepository->getAll();
        $statuses = $this->statusRepository->getStatusesForEntity('posts');
        $view->with('types', $this->types)
            ->with('categories', $categories)
            ->with('tags', $tags)
            ->with('statuses', $statuses);
    }


}