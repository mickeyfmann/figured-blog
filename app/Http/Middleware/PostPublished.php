<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class PostPublished
 *
 * Ensures that a post url is live
 *
 * @package App\Http\Middleware
 */
class PostPublished
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $post = $request->route()->parameter('post');

        if (is_null($post) || $post->status->code != 'published') {
            abort(404);
        }

        return $next($request);
    }
}
