<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Class AdminAccess
 *
 * Ensures only admins access the admin
 *
 * @todo Permissions system would be better but we'll use role for now
 * @package App\Http\Middleware
 */
class AdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if ($user->role->code != 'admin') {
            abort(404);
        }

        return $next($request);
    }
}
