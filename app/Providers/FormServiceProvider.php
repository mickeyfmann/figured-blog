<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class FormServiceProvider
 *
 * Registers a few macros onto the HTML form builder for easier templates
 *
 * @package App\Providers
 */
class FormServiceProvider extends ServiceProvider
{
    public function register(){}

    public function boot()
    {
        $form = $this->app['form'];

        /**
         * Create text field with `form-group` wrapper
         */
        $form->macro('textField', function($name, $label = null, $value = null, $attributes = array()) use ($form) {
            $attributes = array_merge(['class' => 'form-control'], $attributes);
            $element    = $form->text($name, $value, fieldAttributes($name, $attributes));

            return fieldWrapper($name, $label, $element);
        });

        /**
         * Create password field with `form-group` wrapper
         */
        $form->macro('passwordField', function($name, $label = null, $attributes = array()) use ($form) {
            $attributes = array_merge($attributes, ['class' => 'form-control']);
            $element    = $form->password($name, fieldAttributes($name, $attributes));

            return fieldWrapper($name, $label, $element);
        });

        /**
         * Create textarea field with `form-group` wrapper
         */
        $form->macro('textareaField', function($name, $label = null, $value = null, $attributes = array()) use ($form) {
            $attributes = array_merge($attributes, ['class' => 'form-control']);
            $element = $form->textarea($name, $value, fieldAttributes($name, $attributes));

            return fieldWrapper($name, $label, $element);
        });

        /**
         * Create select box with `form-group` wrapper
         */
        $form->macro('selectField', function($name, $label = null, $options, $value = null, $attributes = array(), $helpText = null) use ($form) {
            $attributes = array_merge(['class' => 'form-control select2', 'data-no-matches' => trans('ui/form/form_service_provider.no_matches_found')], $attributes);
            $element = $form->select($name, $options, $value, fieldAttributes($name, $attributes));

            return fieldWrapper($name, $label, $element, $helpText);
        });

        /**
         * Create multi-select field with `form-group` wrapper
         */
        $form->macro('selectMultipleField', function($name, $label = null, $options, $value = null, $attributes = array()) use ($form) {
            $attributes = array_merge($attributes, ['multiple' => true, 'class' => 'form-control select2']);
            $element = $form->select($name, $options, $value, fieldAttributes($name, $attributes));

            return fieldWrapper($name, $label, $element);
        });

        /**
         * Create checkbox field with `checkbox` wrapper
         */
        $form->macro('checkboxField', function($name, $label = null, $value = 1, $checked = null, $attributes = array()) use ($form) {
            $attributes = array_merge(['id' => 'id-field-' . $name], $attributes);

            $out = '<div class="checkbox';
            $out .= fieldError($name) . '">';
            $out .= '<label>';
            $out .= $form->checkbox($name, $value, $checked, $attributes) . ' ' . $label;
            $out .= '</label>';
            $out .= '</div>';

            return $out;
        });

        /**
         * Datepicker
         */
        $form->macro('datepicker', function($name, $label = null, $value = null, $attributes = array()) use ($form) {
            $attributes = array_merge(['class' => 'form-control'], $attributes);
            $element    = $form->text($name, $value, fieldAttributes($name, $attributes));

            return addonWrapper($name, $label, $element);
        });

    }
}