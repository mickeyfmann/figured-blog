<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Permission
 *
 * Access definition model
 *
 * @package App
 */
class Permission extends Model
{
    protected $fillable = ['code', 'name', 'description'];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'role_permissions');
    }
}
