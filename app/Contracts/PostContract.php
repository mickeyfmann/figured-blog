<?php

namespace App\Contracts;

/**
 * Interface PostContract
 *
 * Defines needed methods/attributes for a post model
 *
 * @package App\Contracts
 */
interface PostContract
{
    public function getTypeNameAttribute();

    public function getValidationRules();

    public function getShortAttribute();

    public function getAuthorAttribute();

    public function getFeatureViewAttribute();

    public function getFeatureShortViewAttribute();
}