<?php

namespace App\Contracts;

/**
 * Interface RepositoryContract
 *
 * Definition for basic repository functionality
 *
 * @package App\Contracts
 */
interface RepositoryContract
{
    public function getModel();

    public function find($id);

    public function firstOrCreate(array $attributes);

    public function firstOrNew(array $attributes);

    public function getById($id, array $with = []);

    public function getFirstBy($key, $value, array $with = []);

    public function getManyBy($key, $value, array $with = []);

    public function getFirstByAttributes(array $attributes, array $with = []);

    public function getManyByAttributes(array $attributes, array $with = []);

    public function getRandom(array $with = []);

    public function getRandomBy($key, $value, array $with = []);

    public function getRandomByAttributes(array $attributes, array $with = []);

    public function getAll();

    public function make(array $with = []);

    public function newInstance(array $attributes = []);

    public function create(array $attributes);

    public function destroy($id);

    public function delete();

    public function deleteModel($model);

    public function save($model);

    public function replicate($model);

    public function attach($model, $relationMethod, array $relatedIds);

    public function saveMany($model, $relationMethod, array $relatedModels);

    public function detachAll($model, $relationMethod);

    public function detach($model, $relationMethod, $relatedIds = null, $touch = true);

    public function saveRelated($model, $relationMethod, $relatedModel);

    public function createRelated($model, $relationMethod, array $attributes);

    public function sync($model, $relationMethod, $ids, $detaching = true);

    public function touch($model);

    public function insert (array $data);

    public function load(&$model, array $with);
}