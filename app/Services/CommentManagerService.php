<?php

namespace App\Services;

use App\Comment;
use App\Post;
use App\Repositories\CommentRepository;
use App\Repositories\StatusRepository;

/**
 * Class CommentManager
 *
 * Service interface for comment management
 *
 * @package App\Services
 */
class CommentManagerService
{
    /**
     * @var CommentRepository
     */
    protected $commentRepository;

    /**
     * @var StatusRepository
     */
    protected $statusRepository;

    /**
     * CommentManager constructor.
     *
     * @param CommentRepository $commentRepository
     * @param StatusRepository $statusRepository
     */
    public function __construct(CommentRepository $commentRepository, StatusRepository $statusRepository)
    {
        $this->commentRepository = $commentRepository;
        $this->statusRepository = $statusRepository;
    }

    /**
     * @return CommentRepository
     */
    public function getRepository()
    {
        return $this->commentRepository;
    }

    /**
     * Creates a comment for the provided post with the provided input
     *
     * @param Post $post
     * @param array $inputs
     * @param null $userId
     * @return mixed
     */
    public function createComment(Post $post, array $inputs, $userId = null)
    {
        if (!is_null($userId)) {
            $status = $this->statusRepository->getStatusesForEntity('comments')->where('code', 'published')->first();
        } else {
            $status = $this->statusRepository->getStatusesForEntity('comments')->where('code', 'pending')->first();
        }

        return $this->commentRepository->create([
            'post_id' => $post->id,
            'status_id' => $status->id,
            'user_id'   => $userId,
            'email'     => isset($inputs['email']) ? $inputs['email'] : null,
            'name'      => isset($inputs['name']) ? $inputs['name'] : null,
            'content'   => $inputs['content']
        ]);
    }

    /**
     * Updates a comment status
     *
     * @param Comment $comment
     * @param string $statusCode
     */
    public function updateCommentStatus(Comment $comment, string $statusCode)
    {
        $status = $this->statusRepository->getStatusesForEntity('comments')->where('code', $statusCode)->first();
        $comment->status_id = $status->id;
        $this->commentRepository->save($comment);

        return $comment;
    }

}