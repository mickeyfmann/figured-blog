<?php

namespace App\Services;

use App;
use App\Post;
use App\Repositories\PostRepository;
use App\Repositories\StatusRepository;
use Carbon\Carbon;

/**
 * Class PostManagerService
 *
 * Creation and modifying of posts
 *
 * @package App\Services
 */
class PostManagerService
{
    /**
     * @var PostRepository
     */
    protected $postRepository;

    /**
     * @var StatusRepository
     */
    protected $statusRepository;

    /**
     * PostManagerService constructor.
     *
     * @param PostRepository $postRepository
     * @param StatusRepository $statusRepository
     */
    public function __construct(PostRepository $postRepository, StatusRepository $statusRepository)
    {
        $this->postRepository = $postRepository;
        $this->statusRepository = $statusRepository;
    }

    /**
     * @return PostRepository
     */
    public function getRepository()
    {
        return $this->postRepository;
    }

    /**
     * Creates a new post and attaches related objects
     *
     * @param array $inputs
     * @return mixed
     */
    public function createPost(array $inputs)
    {
        $post = App::make($inputs['type']);
        $post->fill([
            'status_id'      => $inputs['status_id'],
            'title'          => $inputs['title'],
            'slug'           => $inputs['slug'],
            'content'        => $inputs['content'],
            'featured_label' => isset($inputs['feature_label']) ? $inputs['feature_label'] : null,
            'video_link'     => isset($inputs['video_link']) ? $inputs['video_link'] : null,
            'featured_image' => isset($inputs['featured_image']) ? $inputs['featured_image'] : null,
            'gallery_images' => isset($inputs['gallery_images']) ? $inputs['gallery_images'] : null,
            'publish_at'     => isset($inputs['publish_at']) ? Carbon::parse($inputs['publish_at']) : null,
         ]);
        $this->postRepository->save($post);

        $this->postRepository->attach($post, 'categories', $inputs['categories']);
        $this->postRepository->attach($post, 'tags', $inputs['tags']);

        return $post;
    }

    /**
     * Updates the provided post with provided inputs and related objects
     *
     * @param Post $post
     * @param array $inputs
     * @return Post
     */
    public function updatePost(Post $post, array $inputs)
    {
        $post->fill([
            'status_id'      => $inputs['status_id'],
            'title'          => $inputs['title'],
            'slug'           => $inputs['slug'],
            'content'        => $inputs['content'],
            'featured_label' => isset($inputs['feature_label']) ? $inputs['feature_label'] : null,
            'video_link'     => isset($inputs['video_link']) ? $inputs['video_link'] : null,
            'featured_image' => isset($inputs['featured_image']) ? $inputs['featured_image'] : null,
            'gallery_images' => isset($inputs['gallery_images']) ? $inputs['gallery_images'] : null,
            'publish_at'     => isset($inputs['publish_at']) ? Carbon::parse($inputs['publish_at']) : null,
        ]);
        $this->postRepository->save($post);

        $this->postRepository->detachAll($post, 'categories');
        $this->postRepository->attach($post, 'categories', $inputs['categories']);
        $this->postRepository->detachAll($post, 'tags');
        $this->postRepository->attach($post, 'tags', $inputs['tags']);

        return $post;
    }

    /**
     * Sets the provided post to the specified status
     *
     * @param Post $post
     * @param string $statusCode
     * @return Post
     */
    public function updatePostStatus(Post $post, string $statusCode)
    {
        $status = $this->statusRepository->getStatusesForEntity('posts')->where('code', $statusCode)->first();
        $post->status_id = $status->id;
        $this->postRepository->save($post);

        return $post;
    }
}