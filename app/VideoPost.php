<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class VideoPost
 *
 * Video Post model
 *
 * @package App
 */
class VideoPost extends Post
{
    /**
     * Returns the display value for the post type
     *
     * @return string
     */
    public function getTypeNameAttribute()
    {
        return 'Video';
    }

    /**
     * Returns the additional validation rules for the post type
     *
     * @return array
     */
    public function getValidationRules()
    {
        return [
            'video_link' => 'required|url'
        ];
    }

    /**
     * Returns the view for outputting the post feature
     *
     * @return string
     */
    public function getFeatureViewAttribute()
    {
        return 'includes.video-post';
    }
}
