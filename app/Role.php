<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 *
 * System object used to determine user permissions
 *
 * @package App
 */
class Role extends Model
{
    protected $fillable = ['code', 'name', 'description'];

    /**
     * A role can have many permissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany('App\Permission', 'role_permissions');
    }
}
