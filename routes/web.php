<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/**
 * Public Routes
 */
Route::get('/', [
    'uses' => 'BlogController@index',
    'as'   => 'index',
]);

Route::get('/blog/{post}', [
    'uses' => 'BlogController@post',
    'as'   => 'blog.post',
    'middleware' => ['post.published'],
]);

Route::get('/blog/category/{category}', [
    'uses' => 'BlogController@category',
    'as'   => 'blog.category',
]);

Route::get('/blog/tag/{tag}', [
    'uses' => 'BlogController@tag',
    'as'   => 'blog.tag'
]);

Route::post('/blog/comment/{post}', [
    'uses' => 'BlogController@comment',
    'as'   => 'blog.comment'
]);

/**
 * Admin Routes
 */
Route::group([
    'namespace'  => '\\Admin',
    'middleware' => ['auth', 'admin.access'],
    'prefix'     => 'admin'
], function () {

    Route::group(['prefix' => 'posts'], function () {

        Route::get('/', [
            'uses' => 'PostController@index',
            'as'   => 'admin.posts.list'
        ]);

        Route::get('/create', [
            'uses' => 'PostController@create',
            'as'   => 'admin.posts.create'
        ]);

        Route::post('/', [
            'uses' => 'PostController@store',
            'as'   => 'admin.posts.store',
        ]);

        Route::get('/{post}', [
            'uses' => 'PostController@edit',
            'as'   => 'admin.posts.edit',
        ]);

        Route::patch('/{post}', [
            'uses' => 'PostController@update',
            'as'   => 'admin.posts.update',
        ]);

        Route::get('/{post}/remove', [
            'uses' => 'PostController@destroy',
            'as'   => 'admin.posts.remove',
        ]);

        Route::get('/{post}/publish', [
            'uses' => 'PostController@publish',
            'as'   => 'admin.posts.publish',
        ]);

        Route::get('/{post}/trash', [
            'uses' => 'PostController@trash',
            'as'   => 'admin.posts.trash',
        ]);

    });

    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', [
            'uses' => 'CategoryController@index',
            'as'   => 'admin.categories.list',
        ]);

        Route::get('/create', [
            'uses' => 'CategoryController@create',
            'as'   => 'admin.categories.create',
        ]);

        Route::post('/', [
            'uses' => 'CategoryController@store',
            'as'   => 'admin.categories.store',
        ]);

        Route::get('/{category}', [
            'uses' => 'CategoryController@edit',
            'as'   => 'admin.categories.edit',
        ]);

        Route::patch('/{category}', [
            'uses' => 'CategoryController@update',
            'as'   => 'admin.categories.update',
        ]);

        Route::get('/{category}/remove', [
            'uses' => 'CategoryController@destroy',
            'as'   => 'admin.categories.remove',
        ]);
    });

    Route::group(['prefix' => 'tags'], function () {
        Route::get('/', [
            'uses' => 'TagController@index',
            'as'   => 'admin.tags.list',
        ]);

        Route::get('/create', [
            'uses' => 'TagController@create',
            'as'   => 'admin.tags.create',
        ]);

        Route::post('/', [
            'uses' => 'TagController@store',
            'as'   => 'admin.tags.store',
        ]);

        Route::get('/{tag}', [
            'uses' => 'TagController@edit',
            'as'   => 'admin.tags.edit',
        ]);

        Route::patch('/{tag}', [
            'uses' => 'TagController@update',
            'as'   => 'admin.tags.update',
        ]);

        Route::get('/{tag}/remove', [
            'uses' => 'TagController@destroy',
            'as'   => 'admin.tags.remove',
        ]);
    });

    Route::group(['prefix' => 'comments'], function () {
       Route::get('/', [
           'uses' => 'CommentController@index',
           'as'   => 'admin.comments.list',
       ]);

       Route::get('/{comment}/publish', [
           'uses' => 'CommentController@publish',
           'as'   => 'admin.comments.publish',
       ]);

       Route::get('/{comment}/spam', [
           'uses' => 'CommentController@spam',
           'as'   => 'admin.comments.spam',
       ]);

       Route::get('/{comment}/trash', [
           'uses' => 'CommentController@trash',
           'as'   => 'admin.comments.trash',
       ]);

       Route::get('/{comment}/remove', [
           'uses' => 'CommentController@destroy',
           'as'   => 'admin.comments.remove',
       ]);
    });
});

/**
 * API Routes
 */
Route::group([
    'namespace'  => '\\Api',
    'middleware' => [],
    'prefix'     => 'api',
], function () {

    Route::get('/posts', [
        'uses' => 'PostController@index',
        'as'   => 'api.posts.list'
    ]);

    Route::get('/categories', [
        'uses' => 'CategoryController@index',
        'as'   => 'api.categories.list',
    ]);

    Route::get('/tags', [
        'uses' => 'TagController@index',
        'as'   => 'api.tags.list',
    ]);

    Route::get('/comments', [
        'uses' => 'CommentController@index',
        'as'   => 'api.comments.list',
    ]);
});

