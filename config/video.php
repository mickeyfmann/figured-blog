<?php

/**
 * Video Embed Params
 */
return [
    'whitelist' => ['YouTube', 'Vimeo'],
    'params' => [
        'autoplay' => 1,
        'loop' => 0,
    ],
];