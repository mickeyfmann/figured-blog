<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = collect([
            [
                'code' => 'admin',
                'name' => 'Admin',
                'description' => 'System Administrator',
            ],
            [
                'code' => 'guest',
                'name' => 'Guest',
                'description' => 'Unlogged in visitor',
            ],
            [
                'code' => 'subscriber',
                'name' => 'Subscriber',
                'description' => 'Registered User',
            ]
        ]);

        $roles->each(function ($role) {
            Role::create($role);
        });
    }
}
