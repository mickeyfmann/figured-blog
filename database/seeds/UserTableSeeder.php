<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = collect([
            [
                'role_id' => 1,
                'name' => 'Mickey Freeman',
                'password' => bcrypt('password'),
                'email' => 'mickeyfmann@gmail.com',
            ],
            [
                'role_id' => 1,
                'name' => 'Admin',
                'email' => 'admin',
                'password' => bcrypt('password')
            ]
        ]);

        $users->each(function ($user) {
            User::create($user);
        });
    }
}
