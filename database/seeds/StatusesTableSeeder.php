<?php

use App\Status;
use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = collect([
            [
                'code' => 'draft',
                'name' => 'Draft',
                'description' => 'Object is in edit or review mode, not available to the public'
            ],
            [
                'code' => 'published',
                'name' => 'Published',
                'description' => 'Object is live and viewable to front-end users',
            ],
            [
                'code' => 'trash',
                'name' => 'Trash',
                'description' => 'Object has been deleted'
            ],
            [
                'code' => 'pending',
                'name' => 'Pending Approval',
                'description' => 'Object is pending adminstrative approval',
            ],
            [
                'code' => 'spam',
                'name' => 'SPAM',
                'description' => 'Object has been flagged as SPAM',
            ]
        ]);

        $statuses->each(function($status) {
            Status::create($status);
        });
    }
}
