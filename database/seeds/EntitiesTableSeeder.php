<?php

use App\Entity;
use Illuminate\Database\Seeder;


class EntitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entities = collect([
            [
                'code' => 'posts',
                'name' => 'Posts',
                'description' => 'Blog post object'
            ],
            [
                'code' => 'comments',
                'name' => 'Comments',
                'description' => 'Post comments or responses',
            ],
        ]);

        $entities->each(function ($entity) {
           Entity::create($entity);
        });
    }
}
