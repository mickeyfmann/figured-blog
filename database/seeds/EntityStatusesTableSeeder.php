<?php

use App\Entity;
use App\Status;
use Illuminate\Database\Seeder;

class EntityStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $postEntity = Entity::where('code', 'posts')->first();
        $commentEntity = Entity::where('code', 'comments')->first();

        $postStatuses = Status::whereIn('code', ['draft', 'published', 'trash'])->get()->pluck('id');
        $commentStatuses = Status::whereIn('code', ['pending', 'published', 'spam', 'trash'])->get()->pluck('id');

        $ordinal = 1;
        $postStatuses->each(function ($status) use ($postEntity, &$ordinal) {
            $postEntity->statuses()->attach($status, ['ordinal' => $ordinal]);
            $ordinal++;
        });

        $ordinal = 1;
        $commentStatuses->each(function ($status) use ($commentEntity, &$ordinal) {
           $commentEntity->statuses()->attach($status, ['ordinal' => $ordinal]);
           $ordinal++;
        });
    }
}
