<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'id' => $faker->randomNumber(),
        'email' => $faker->email,
        'name' => $faker->name(),
        'content' => $faker->paragraph()
    ];
});
