<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'id'        => $faker->randomNumber(),
        'status_id' => 1,
        'title'     => $faker->word,
        'slug'      => $faker->slug,
        'content'   => $faker->paragraphs(5, true),
    ];
});
