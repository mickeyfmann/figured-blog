<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_status', function (Blueprint $table) {
            $table->unsignedInteger('entity_id');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('ordinal')->default(1);

            $table->primary(['entity_id', 'status_id']);

            $table->foreign('entity_id')
                ->references('id')
                ->on('entities')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');

            $table->foreign('status_id')
                ->references('id')
                ->on('statuses')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');
        });

        Artisan::call('db:seed', [
            '--class' => 'EntityStatusesTableSeeder',
            '--force' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_status', function (Blueprint $table) {
            $table->dropForeign('entity_statuses_entity_id_foreign');
            $table->dropForeign('entity_statuses_status_id_foreign');
        });
        Schema::dropIfExists('entity_statuses');
    }
}
