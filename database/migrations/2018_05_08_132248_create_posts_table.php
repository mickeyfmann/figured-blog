<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->unsignedInteger('status_id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('content');
            $table->string('feature_label')->nullable();
            $table->string('video_link')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamp('publish_at')->nullable();
            $table->timestamps();

            $table->foreign('status_id')
                ->references('id')
                ->on('statuses')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');

            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('SET NULL')
                ->onUpdate('CASCADE');

            $table->foreign('updated_by')
                ->references('id')
                ->on('users')
                ->onDelete('SET NULL')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /**
         * Drop foreign key constraints for easy database resets
         */
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign('posts_status_id_foreign');
            $table->dropForeign('posts_created_by_foreign');
            $table->dropForeign('posts_updated_by_foreign');
        });

        Schema::dropIfExists('posts');
    }
}
