<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_images', function (Blueprint $table) {
            $table->unsignedInteger('post_id');
            $table->unsignedInteger('image_id');
            $table->string('type');
            $table->string('caption')->nullable();
            $table->unsignedInteger('ordinal')->default(1);

            $table->primary(['post_id', 'image_id']);

            $table->foreign('post_id')
                ->references('id')
                ->on('posts')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table->foreign('image_id')
                ->references('id')
                ->on('images')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_images', function (Blueprint $table) {
            $table->dropForeign('post_images_post_id_foreign');
            $table->dropForeign('post_images_image_id_foreign');
        });
        Schema::dropIfExists('post_images');
    }
}
