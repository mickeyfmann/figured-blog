@extends('layouts.master')

@section('page-title', 'Posts')

@section('headline', 'Posts')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                @foreach ($posts as $post)
                    @include('includes.post-summary', ['post' => $post])
                @endforeach
            </div>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Categories
                    </div>
                    <div class="panel-body">
                        @foreach($categories as $category)
                            <a href="{{ route('blog.category', $category) }}">{{ $category->name }}</a><br />
                        @endforeach
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Recent Posts
                    </div>
                    <div class="panel-body">
                        @foreach($recentPosts as $post)
                            <a href="{{ route('blog.post', $post) }}">{{ $post->title }}</a><br />
                        @endforeach
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Tags
                    </div>
                    <div class="panel-body">
                        @foreach($tags as $tag)
                            <a href="{{ route('blog.tag', $tag) }}">{{ $tag->name }}</a><br />
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@append