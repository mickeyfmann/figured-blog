@extends('layouts.master')

@section('page-title', $category->name)

@section('headline', $category->name)

@section('content')
    @foreach ($posts as $post)
        @include('includes.post-summary', ['post' => $post])
    @endforeach
@append