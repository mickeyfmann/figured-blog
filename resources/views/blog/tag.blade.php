@extends('layouts.master')

@section('page-title', $tag->name)

@section('headline', $tag->name)

@section('content')
    @foreach ($posts as $post)
        @include('includes.post-summary', ['post' => $post])
    @endforeach
@append