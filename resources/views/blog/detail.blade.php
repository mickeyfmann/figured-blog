@extends('layouts.master')

@section('page-title', $post->title)

@section('headline', $post->title)

@section('content')
    <div id="main-blog-single">
        <div class="content">
            <div>
                <div class="row">
                    <div class="blog-main col-sm-8">
                        @include($post->feature_view, ['post' => $post])
                    </div>
                    @include('includes.blog-bar', ['post' => $post])
                    <div class="post-content">
                        {!! $post->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div><hr/></div>
    @if ($post->comments->where('status.code', 'published')->count() > 0)
        <div class="row">
            <h3>Comments</h3>
            <br/>
            <br/>
            @foreach($post->comments->where('status.code', 'published') as $comment)
                <div class="comment">
                    @if (!empty($comment->user))
                        <h4>{{ $comment->user->name }}</h4>
                    @else
                        <h4>{{ $comment->name }}</h4>
                    @endif
                    <small>{{ $comment->created_at->format('F jS Y') }}</small>
                    <hr/>
                    <p>{{ $comment->content }}</p>
                </div>
            @endforeach
        </div>
    @endif
    <div><hr/></div>
    <div class="row">
        {!! Form::open(['url' => route('blog.comment', $post), 'class' => 'form', 'role' => 'form', 'method' => 'post']) !!}
            @if(auth()->check())
                Logged in as {{ auth()->user()->name }}<br />
            @else
                {!! Form::textField('email', 'Email') !!}
                {!! Form::textField('name', 'Name') !!}
            @endif
            <h3>Comment</h3>
            {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
            <button type="submit" class="btn btn-primary btn-addon">
                <i class="fa fa-save"></i> Leave Comment
            </button>
        {!! Form::close() !!}
    </div>
@append