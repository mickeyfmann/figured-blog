{{--
===================
GLOBAL SITE MESSAGE
===================
@todo Blow this out to allow for message types
info, warning, danger, etc.
--}}
@if(app('session')->has('message'))
    <div class="global-message alert alert-info alert-dismissable fade in fade-in" role="alert">
        <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
        {{ session('message') }}
    </div>
@elseif(app('session')->has('message-success'))
    <div class="global-message alert alert-success alert-dismissable fade in fade-in" role="alert">
        <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
        {{ session('message-success') }}
    </div>
@endif