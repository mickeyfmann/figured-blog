<div class="row">
    <div class="blog-main col-sm-8">
        <div class="blog-cats-list">
            <div class="image">
                @include($post->feature_short_view)
            </div>
            <h3><a href="{{ route('blog.post', $post) }}">{{ $post->title }}</a></h3>
            @include('includes.blog-bar', ['post' => $post])
            {!! $post->short !!}
            <br />
            <hr />
        </div>
    </div>
</div>