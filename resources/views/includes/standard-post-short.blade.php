@if ($post->featured_image != null)
    <div class="blog-main-image">
        <div class="single-post-format">
            <img src="{{ url($post->featured_image) }}" class="attachment-full size-full wp-post-image"/>
        </div>
    </div>
@endif