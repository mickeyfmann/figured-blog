<div class="blog-main-image">
    <div class="single-post-format">
        {!! \Merujan99\LaravelVideoEmbed\Facades\LaravelVideoEmbed::parse($post->video_link, config('video.whitelist'), config('video.params')) !!}
    </div>
</div>