<div class="blog-bottom-info">
    <ul>
        <li>
            <i class="fa fa-calendar"></i>
            <a href="{{ route('blog.post', $post) }}">{{ $post->displayDate }}</a>
        </li>
        <li>
            <i class="fa fa-comments"></i>
            {{ $post->commentCount }} Comments
        </li>
        <li>
            <i class="fa fa-tags"></i>
        </li>
    </ul>
    <div id="post-author">
        <i class="fa fa-pencil"></i>
        By {{ $post->author }}
    </div>
</div>