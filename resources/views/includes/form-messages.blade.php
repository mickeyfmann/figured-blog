@if(isset($errors) && $errors->any())
    <div class="alert alert-warning">
        @foreach ($errors->all('<p>:message</p>') as $message)
            {!! $message !!}
        @endforeach
    </div>
@endif
