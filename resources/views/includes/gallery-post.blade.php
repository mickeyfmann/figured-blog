<div class="blog-main-image gallery-out">
    <div class="single-post-format">
        @foreach($post->gallery_images as $image)
            <div class="responsive">
                <div class="gallery">
                    <a target="_blank" href="{{ url($image) }}">
                        <img src="{{ url($image) }}" />
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</div><br/>
<br/>