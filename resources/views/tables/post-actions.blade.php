
<a href="{{ route('admin.posts.edit', $model) }}">Edit</a> |
<a href="{{ route('blog.post', $model) }}">View</a> |
@if ($model->status->code != 'published')
    <a href="{{ route('admin.posts.publish', $model) }}">Publish</a>
@else
    <a href="{{ route('admin.posts.trash', $model) }}">Trash</a>
@endif |
<a href="{{ route('admin.posts.remove', $model) }}">Delete</a>