@extends('layouts.app')

@section('page-title', 'Posts Admin')

@section('headline', 'Posts Admin')

@section('content')
    <div>
        <a class="btn btn-primary" href="{{ route('admin.posts.create') }}">New Post</a>
    </div>
    <table class="table table-bordered" id="posts-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Type</th>
                <th>Status</th>
                <th>Title</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Actions</th>
            </tr>
        </thead>
    </table>
@append
@section('css-src')
    <link href="{{ asset('css/jquery.dataTables.css') }}" rel="stylesheet">
@append
@section('script-src')
    <script src="{{ asset('js/jquery.dataTables.js') }}"></script>
@append
@section('scripts')
<script>
    $(function() {
        $('#posts-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('api.posts.list') !!}',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'type', name: 'type'},
                {data: 'status_id', name: 'status_id'},
                {data: 'title', name: 'title'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'action', name: 'action'}
            ]
        });
    });
</script>
@append