<div class="container" id="post-container" data-title="{{ isset($post) ? $post->title : null }}" data-slug="{{ isset($post) ? $post->slug : null }}" data-type="{{ isset($post) ? $post->type : array_keys($types)[0] }}">
    {!! Form::open(['url' => $route, 'class' => 'form', 'role' => 'form', 'method' => $method]) !!}
        <div class="row">
            <div class="col-md-8">
                @include('includes.form-messages')
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::textField('title', 'Title', null, ['v-model' => 'title']) !!}
                        {!! Form::textField('slug', 'Slug', null, ['v-model' => 'slug']) !!}
                        <div class="form-group">
                            {!! Form::label('status_id', 'Status') !!}<br />
                            {!! Form::select('status_id', $statuses->pluck('name', 'id')->all(), isset($post) ? $post->status_id : null) !!}
                        </div>
                        <h3>Content</h3>
                        {!! Form::textarea('content', isset($post) ? $post->content : null, ['class' => 'form-control', 'id' => 'content']) !!}
                    </div>
                </div>
                <br />
                <div class="row" id="gallery" v-if="type === 'App\\GalleryPost'">
                    <div class="col-md-12">
                        {!! Form::textField('feature_label', 'Label', isset($post) ? $post->feature_label : null) !!}
                        {!! Form::label('gallery_images', 'Images') !!}<br />
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a id="lfm-1" data-input="thumbnail-1" data-preview="holder-1" class="btn btn-primary">
                                    <i class="fa fa-picture-o"></i> Choose
                                </a>
                            </span>
                            <input id="thumbnail-1" class="form-control" type="text" name="gallery_images[]">
                        </div>
                        <img id="holder-1" style="margin-top:15px;max-height:100px;"><Br />
                        <br />
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a id="lfm-2" data-input="thumbnail-2" data-preview="holder-2" class="btn btn-primary">
                                    <i class="fa fa-picture-o"></i> Choose
                                </a>
                            </span>
                            <input id="thumbnail-2" class="form-control" type="text" name="gallery_images[]">
                        </div>
                        <img id="holder-2" style="margin-top:15px;max-height:100px;"><br />
                        <br />
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a id="lfm-3" data-input="thumbnail-3" data-preview="holder-3" class="btn btn-primary">
                                    <i class="fa fa-picture-o"></i> Choose
                                </a>
                            </span>
                            <input id="thumbnail-3" class="form-control" type="text" name="gallery_images[]">
                        </div>
                        <img id="holder-3" style="margin-top:15px;max-height:100px;">
                    </div>
                </div>

                <div class="row" id="video" v-if="type === 'App\\VideoPost'">
                    <div class="col-md-12">
                        {!! Form::textField('feature_label', 'Label', isset($post) ? $post->feature_label : null) !!}
                        {!! Form::textField('video_link', 'Video Link', isset($post) ? $post->video_link : null) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary btn-addon">
                            <i class="fa fa-save"></i> Save
                        </button>
                        <a class="btn btn-default" href="{{ route('admin.posts.list') }}">Cancel</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Publish At
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('publish_at', 'Publish this post at the following date/time') !!}<br />
                            {!! Form::datetime('publish_at', isset($post) ? $post->publish_at : null) !!}
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Format
                    </div>
                    <div class="panel-body">
                        @if(isset($post))
                            {{ $post->type_name }}
                        @else 
                            @foreach($types as $class => $type)
                                <input type="radio" name="type" id="post-format-{{ strtolower($type) }}" value="{{ $class }}" v-model="type" @if($type == 'Gallery') onclick="setTimeout(fileManagerSetup, 1000)" @endif/>
                                {!! Form::label('post-format-' . strtolower($type), $type) !!} <br />
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Categories
                    </div>
                    <div class="panel-body">
                        @foreach($categories as $category)
                            {!! Form::checkbox('categories[]', $category->id, (isset($post) && $post->categories->pluck('id')->contains($category->id)), ['id' => 'category-' . $category->id]) !!}
                            {!! Form::label('category-' . $category->id, $category->name) !!} <br />
                        @endforeach
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Tags
                    </div>
                    <div class="panel-body">
                        @foreach($tags as $tag)
                            {!! Form::checkbox('tags[]', $tag->id, (isset($post) && $post->tags->pluck('id')->contains($tag->id)), ['id' => 'tag-' . $tag->id]) !!}
                            {!! Form::label('tag-' . $tag->id, $tag->name) !!} <br />
                        @endforeach
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Featured Image
                    </div>
                    <div class="panel-body">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                    <i class="fa fa-picture-o"></i> Choose
                                </a>
                            </span>
                            <input id="thumbnail" class="form-control" type="text" name="featured_image">
                        </div>
                        <img id="holder" style="margin-top:15px;max-height:100px;">
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</div>
@section('script-src')
    {!! Html::script('js/ckeditor/ckeditor.js') !!}
    {!! Html::script('vendor/laravel-filemanager/js/lfm.js') !!}
@append
@section('scripts')
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        };
        CKEDITOR.replace( 'content', options );

        var fileManagerSetup = function (event) {
            $('#lfm-1').filemanager('image');
            $('#lfm-2').filemanager('image');
            $('#lfm-3').filemanager('image');
        }

        $(document).ready(function() {
            $('#lfm').filemanager('image');
        });
    </script>
@append