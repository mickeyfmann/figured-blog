@extends('layouts.app')

@section('page-title', 'Edit Post')

@section('headline', 'Edit Post')

@section('content')
    <div>
        @include('admin.posts.form')
    </div>
@append