@extends('layouts.app')

@section('page-title', 'New Post')

@section('headline', 'New Post')

@section('content')
    <div>
        @include('admin.posts.form')
    </div>
@append