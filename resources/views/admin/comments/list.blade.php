@extends('layouts.app')

@section('page-title', 'Comment Admin')

@section('headline', 'Comment Admin')

@section('content')
    <table class="table table-bordered" id="tag-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Post</th>
                <th>Status</th>
                <th>User</th>
                <th>Email</th>
                <th>Name</th>
                <th>Content</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Actions</th>
            </tr>
        </thead>
    </table>
@append
@section('css-src')
    <link href="{{ asset('css/jquery.dataTables.css') }}" rel="stylesheet">
@append
@section('script-src')
    <script src="{{ asset('js/jquery.dataTables.js') }}"></script>
@append
@section('scripts')
<script>
    $(function() {
        $('#tag-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('api.comments.list') !!}',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'post_id', name: 'post_id'},
                {data: 'status_id', name: 'status_id'},
                {data: 'user_id', name: 'user_id'},
                {data: 'email', name: 'email'},
                {data: 'name', name: 'name'},
                {data: 'content', name: 'content'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'action', name: 'action'}
            ]
        });
    });
</script>
@append