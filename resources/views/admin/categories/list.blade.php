@extends('layouts.app')

@section('page-title', 'Category Admin')

@section('headline', 'Category Admin')

@section('content')
    <div>
        <a class="btn btn-primary" href="{{ route('admin.categories.create') }}">New Category</a>
    </div>
    <table class="table table-bordered" id="category-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Code</th>
                <th>Name</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Actions</th>
            </tr>
        </thead>
    </table>
@append

@section('css-src')
    <link href="{{ asset('css/jquery.dataTables.css') }}" rel="stylesheet">
@append
@section('script-src')
    <script src="{{ asset('js/jquery.dataTables.js') }}"></script>
@append
@section('scripts')
<script>
    $(function() {
        $('#category-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('api.categories.list') !!}',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'code', name: 'code'},
                {data: 'name', name: 'name'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'action', name: 'action'}
            ]
        });
    });
</script>
@append