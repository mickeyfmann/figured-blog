<div class="container">
    {!! Form::open(['url' => $route, 'class' => 'form', 'role' => 'form', 'method' => $method]) !!}
        @include('includes.form-messages')
        <div class="row">
            <div class="col-md-12">
                {!! Form::textField('code', 'Code', isset($category) ? $category->code : null) !!}
                {!! Form::textField('name', 'Name', isset($category) ? $category->name : null) !!}
                <h3>Description</h3>
                {!! Form::textarea('description', isset($category) ? $category->description : null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <br />
        <div class="row">
            <button type="submit" class="btn btn-primary btn-addon">
                <i class="fa fa-save"></i> Save
            </button>
            <a class="btn btn-default" href="{{ route('admin.categories.list') }}">Cancel</a>
        </div>
    {!! Form::close(); !!}
</div>
