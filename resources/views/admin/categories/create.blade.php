@extends('layouts.app')

@section('page-title', 'New Category')

@section('headline', 'New Category')

@section('content')
    <div>
        @include('admin.categories.form')
    </div>
@append