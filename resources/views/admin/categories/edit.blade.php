@extends('layouts.app')

@section('page-title', 'Edit Category')

@section('headline', 'Edit Category')

@section('content')
    <div>
        @include('admin.categories.form');
    </div>
@append