@extends('layouts.app')

@section('page-title', 'Edit Tag')

@section('headline', 'Edit Tag')

@section('content')
    <div>
        @include('admin.tags.form')
    </div>
@append