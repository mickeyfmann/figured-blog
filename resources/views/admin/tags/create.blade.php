@extends('layouts.app')

@section('page-title', 'New Tag')

@section('headline', 'New Tag')

@section('content')
    <div>
        @include('admin.tags.form')
    </div>
@append