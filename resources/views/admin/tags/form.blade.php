<div class="container">
    {!! Form::open(['url' => $route, 'class' => 'form', 'role' => 'form', 'method' => $method]) !!}
        @include('includes.form-messages')
        <div class="row">
            <div class="col-md-12">
                {!! Form::textField('name', 'Name', isset($tag) ? $tag->name : null) !!}
            </div>
        </div>
        <br />
        <div class="row">
            <button type="submit" class="btn btn-primary btn-addon">
                <i class="fa fa-save"></i> Save
            </button>
            <a class="btn btn-default" href="{{ route('admin.tags.list') }}">Cancel</a>
        </div>
    {!! Form::close(); !!}
</div>
