@extends('layouts.app')

@section('page-title', 'Tag Admin')

@section('headline', 'Tag Admin')

@section('content')
    <div>
        <a class="btn btn-primary" href="{{ route('admin.tags.create') }}">New Tag</a>
    </div>
    <table class="table table-bordered" id="tag-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Actions</th>
            </tr>
        </thead>
    </table>
@append
@section('css-src')
    <link href="{{ asset('css/jquery.dataTables.css') }}" rel="stylesheet">
@append
@section('script-src')
    <script src="{{ asset('js/jquery.dataTables.js') }}"></script>
@append
@section('scripts')
<script>
    $(function() {
        $('#tag-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('api.tags.list') !!}',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'action', name: 'action'}
            ]
        });
    });
</script>
@append