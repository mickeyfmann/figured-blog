<?php

namespace Tests\Unit;

use App;
use App\Post;
use App\Repositories\PostRepository;
use App\Repositories\StatusRepository;
use App\Services\PostManagerService;
use Mockery;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class PostManagerServiceTest
 *
 * Test for post management service
 *
 * @package Tests\Unit
 */
class PostManagerServiceTest extends TestCase
{
    /**
     * @var Mockery\MockInterface
     */
    protected $postRepository;

    /**
     * @var Mockery\MockInterface
     */
    protected $statusRepository;

    /**
     * Setup Tests
     */
    public function setUp()
    {
        parent::setUp();

        $this->postRepository = Mockery::mock(PostRepository::class);
        $this->statusRepository = Mockery::mock(StatusRepository::class);
    }

    /**
     * Test post creation
     */
    public function testCreatePost()
    {
        $post = factory(Post::class)->make(['id' => null]);
        $categories = [1, 2, 3];
        $tags = [1, 2];

        App::shouldReceive('make')->once()->with(Post::class)->andReturn($post);

        $this->postRepository->shouldReceive('save')->once()->with($post);
        $this->postRepository->shouldReceive('attach')->once()->with($post, 'categories', $categories);
        $this->postRepository->shouldReceive('attach')->once()->with($post, 'tags', $tags);

        $object = $this->buildObject();
        $result = $object->createPost(array_merge($post->toArray(), [
            'categories' => $categories,
            'tags'       => $tags,
            'type'       => Post::class
        ]));

        $this->assertEquals($post, $result);
        $this->assertEquals($post->title, $result->title);
    }

    /**
     * Test updating a post
     */
    public function testUpdatePost()
    {
        $post = factory(Post::class)->make();
        $categories = [1, 2, 3];
        $tags = [1, 2];

        $inputs = [
            'status_id' => 2,
            'title'     => 'Mickey',
            'slug'      => 'mickey',
            'content'   => 'Mickey Test',
        ];

        $this->postRepository->shouldReceive('save')->once()->withArgs(function ($model) use ($inputs) {
            $this->assertInstanceOf(Post::class, $model);
            $this->assertEquals($inputs['status_id'], $model->status_id);
            $this->assertEquals($inputs['title'], $model->title);
            $this->assertEquals($inputs['slug'], $model->slug);
            $this->assertEquals($inputs['content'], $model->content);
            return true;
        });
        $this->postRepository->shouldReceive('detachAll')->once()->with(Mockery::any(), 'categories');
        $this->postRepository->shouldReceive('attach')->once()->with(Mockery::any(), 'categories', $categories);
        $this->postRepository->shouldReceive('detachAll')->once()->with(Mockery::any(), 'tags');
        $this->postRepository->shouldReceive('attach')->once()->with(Mockery::any(), 'tags', $tags);

        $object = $this->buildObject();
        $result = $object->updatePost($post, array_merge($inputs, [
            'categories' => $categories,
            'tags' => $tags,
        ]));

        $this->assertEquals($inputs['status_id'], $result->status_id);
        $this->assertEquals($inputs['title'], $result->title);
        $this->assertEquals($inputs['slug'], $result->slug);
        $this->assertEquals($inputs['content'], $result->content);
    }

    /**
     * Test publishing a post
     */
    public function testPublishPost()
    {
        $post = factory(Post::class)->make();
        $status = new \stdClass();
        $status->id = 2;

        $this->statusRepository->shouldReceive('getStatusesForEntity->where->first')->once()->andReturn($status);
        $this->postRepository->shouldReceive('save')->once();

        $object = $this->buildObject();
        $result = $object->updatePostStatus($post, 'published');

        $this->assertEquals($status->id, $result->status_id);

    }

    /**
     * Test trashing a post
     */
    public function testTrashPost()
    {
        $post = factory(Post::class)->make();
        $status = new \stdClass();
        $status->id = 3;

        $this->statusRepository->shouldReceive('getStatusesForEntity->where->first')->once()->andReturn($status);
        $this->postRepository->shouldReceive('save')->once();

        $object = $this->buildObject();
        $result = $object->updatePostStatus($post, 'trash');

        $this->assertEquals($status->id, $result->status_id);
    }

    protected function buildObject($partialMock = false)
    {
        if ($partialMock) {
            return Mockery::mock(PostManagerService::class, [$this->postRepository, $this->statusRepository])->makePartial();
        }

        return new PostManagerService($this->postRepository, $this->statusRepository);
    }
}
