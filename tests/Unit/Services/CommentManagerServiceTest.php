<?php

namespace Tests\Unit;

use App\Comment;
use App\Post;
use App\Repositories\CommentRepository;
use App\Repositories\StatusRepository;
use App\Services\CommentManagerService;
use Tests\TestCase;
use Mockery;

class CommentManagerServiceTest extends TestCase
{
    /**
     * @var Mockery\MockInterface
     */
    protected $commentRepository;

    /**
     * @var Mockery\MockInterface
     */
    protected $statusRepository;

    /**
     * Test SEtup
     */
    public function setUp()
    {
        parent::setUp();

        $this->commentRepository = Mockery::mock(CommentRepository::class);
        $this->statusRepository = Mockery::mock(StatusRepository::class);
    }

    public function testCreateCommentWithUserId()
    {
        $post = factory(Post::class)->make();
        $status = new \stdClass();
        $status->id = 3;
        $userId = 2;
        $attributes = [
            'content' => 'This is a test',
        ];
        $this->statusRepository->shouldReceive('getStatusesForEntity->where->first')->once()->andReturn($status);
        $this->commentRepository->shouldReceive('create')->once()->withArgs(function (array $data) use ($post, $status, $userId, $attributes) {
            $this->assertEquals($post->id, $data['post_id']);
            $this->assertEquals($status->id, $data['status_id']);
            $this->assertEquals($userId, $data['user_id']);
            $this->assertEquals($attributes['content'], $data['content']);
            return true;
        })->andReturn(true);

        $object = $this->buildObject();
        $result = $object->createComment($post, $attributes, $userId);

        $this->assertTrue($result);
    }

    public function testCreateCommentWithoutUserId()
    {
        $post = factory(Post::class)->make();
        $status = new \stdClass();
        $status->id = 3;
        $userId = 2;
        $attributes = [
            'content' => 'This is a test',
            'name' => 'Mickey',
            'email' => 'test@test.com',
        ];

        $this->statusRepository->shouldReceive('getStatusesForEntity->where->first')->once()->andReturn($status);
        $this->commentRepository->shouldReceive('create')->once()->withArgs(function (array $data) use ($post, $status, $attributes) {
            $this->assertEquals($post->id, $data['post_id']);
            $this->assertEquals($status->id, $data['status_id']);
            $this->assertEquals($attributes['name'], $data['name']);
            $this->assertEquals($attributes['email'], $data['email']);
            $this->assertEquals($attributes['content'], $data['content']);
            return true;
        })->andReturn(true);

        $object = $this->buildObject();
        $result = $object->createComment($post, $attributes, $userId);

        $this->assertTrue($result);
    }

    public function testUpdateCommentStatus()
    {
        $comment = factory(Comment::class)->make(['status_id' => 1]);
        $status = new \stdClass();
        $status->id = 5;

        $this->statusRepository->shouldReceive('getStatusesForEntity->where->first')->once()->andReturn($status);
        $this->commentRepository->shouldReceive('save')->once();

        $object = $this->buildObject();
        $result = $object->updateCommentStatus($comment, 'published');

        $this->assertEquals($status->id, $result->status_id);
        $this->assertNotEquals(1, $result->status_id);

    }

    protected function buildObject()
    {
        return new CommentManagerService($this->commentRepository, $this->statusRepository);
    }
}