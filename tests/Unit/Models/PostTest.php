<?php

namespace Tests\Unit\Models;

use App\Model;
use App\GalleryPost;
use App\Post;
use App\User;
use Faker\Factory as Faker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class PostTest
 *
 * Test for some of the custom functionality in the posts model
 *
 * @package Tests\Unit\Models
 */
class PostTest extends TestCase
{
    /**
     * @var Faker
     */
    protected $faker;

    public function setUp()
    {
        parent::setUp();
        $this->faker = Faker::create();
    }

    public function testGetShortAttribute()
    {
        $content = $this->faker->paragraphs(6, true);

        $post = new Post();
        $post->content = $content;

        $this->assertNotEquals($content, $post->short);
        $this->assertTrue(str_contains($content, substr($post->short, 0, -3)), 'Failed asserting that short value was contained in original content');
    }

    public function testNewFromBuilderReturnsDifferentModel()
    {
        $attributes = new \stdClass();
        $attributes->id = 1;
        $attributes->type = GalleryPost::class;
        $attributes->title = 'Test';


        $post = new Post();
        $result = $post->newFromBuilder($attributes, null);

        $this->assertInstanceOf(GalleryPost::class, $result);
    }

    public function testCreatedByReturnsAdmin()
    {
        $post = new Post();

        $this->assertEquals('Admin', $post->author);
    }

    public function testCreatedByReturnsCreator()
    {
        $user = new User([
            'id' => 1,
            'name' => 'Test User',
            'email' => $this->faker->email,
        ]);

        $post = new Post([
            'id' => 1,
            'title' => 'Test Title'
        ]);
        $post->created_by = 1;
        $post->createdBy = $user;

        $this->assertEquals('Test User', $post->author);
    }

}
